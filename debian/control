Source: pythondialog
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Florent Rougon <f.rougon@free.fr>,
Homepage: https://pythondialog.sourceforge.net/
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 python3-all,
 python3-doc,
 python3-docutils,
 python3-pygments,
 python3-setuptools,
 python3-sphinx,
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/python-team/packages/pythondialog.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pythondialog
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: python3-dialog
Multi-Arch: foreign
Architecture: all
Built-Using: ${sphinxdoc:Built-Using}
Depends: dialog, ${misc:Depends}, ${python3:Depends}, ${sphinxdoc:Depends}
Suggests: python3-doc
Description: Python module for making simple terminal-based user interfaces
 pythondialog is a Python wrapper for the dialog utility originally
 written by Savio Lam, and later rewritten by Thomas E. Dickey. Its
 purpose is to provide an easy to use, pythonic and comprehensive Python
 interface to dialog. This allows one to make simple text-mode user
 interfaces on Unix-like systems.
 .
 pythondialog provides dialog boxes (widgets) of many different types.
 Among these, one can find infobox, msgbox, yesno, menu, checklist,
 radiolist, fselect (for selecting a file), rangebox, buildlist,
 treeview, calendar. These widgets, and those not listed here, allow one
 to build nice interfaces quickly and easily. However, it is not possible
 to create new widgets without modifying dialog itself.
 .
 For most widgets, the following settings can be chosen:
  * width, height and other parameters where applicable;
  * two user-defined buttons, referred to as "Help" and "Extra", may be
    added and their labels freely chosen.
 Additionally, a color theme may be defined for all widgets via a
 configuration file.
 .
 pythondialog has good Unicode support. Precisely, this support should be
 as good as allowed by the combination of the Python interpreter, dialog,
 the terminal and the locale settings in use.
